package in.silentsudo.apacheavrohw;
// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        // Press Alt+Enter with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        System.out.println("Hello World!");
        Feed socialMedia = new Feed("12121", "Social media");
        System.out.println(socialMedia.get(Feed.SCHEMA$.getFields().get(0).pos()));
    }
}